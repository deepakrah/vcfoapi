﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VcfoApi.Model
{
    public class Services
    {
        public int id { get; set; } = 0;
        public string code { get; set; }
        public string name { get; set; }
        public string category_id { get; set; } = "";
        public string price { get; set; } = "";
        public string categoryName { get; set; } = "";
    }
}
