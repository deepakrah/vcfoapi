﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VcfoApi.Model
{
    public class Orders
    {
        public int id { get; set; } = 0;
        public string order_date { get; set; }
        public decimal amount { get; set; }
        public decimal tax { get; set; }
        public string discount { get; set; }
        public decimal net_amount { get; set; }
        public string MyPmobile_noroperty { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string Status { get; set; }
        public string pg_resCode { get; set; }
        public string pg_resStatus { get; set; }
        public string pg_resMessage { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string mobile_no { get; set; }

        public int UserId { get; set; }
        public string ServiceCode { get; set; }
        public string orderNumber { get; set; }
        public string GSTNo { get; set; } = "";
        public string BusinessName { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string orderStatus { get; set; }
        public string ServiceName { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string EmailId { get; set; }
    }
}
