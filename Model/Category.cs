﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VcfoApi.Model
{
    public class Category
    {
        public int id { get; set; } = 0;
        public string code { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}
