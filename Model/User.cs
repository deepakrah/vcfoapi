﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VcfoApi.Model
{
    public class User
    {
        public int UserId { get; set; } = 0;
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string LoginID { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int StateId { get; set; } = 0;
        public int CountryId { get; set; } = 0;
        public string UserType { get; set; }
        public string Token { get; set; }
        public string CreatedDate { get; set; }
        public string OTP { get; set; }
        public string[] FileSource { get; set; }
        public int OrderId { get; set; } = 0;
    }
    public class OtpLog
    {
        public int OtpLogId { get; set; }
        public string Mobile { get; set; }
        public string OTP { get; set; }
        public string SessionId { get; set; }

    }

    public class LookupDoc
    {
        public int LookupDocId { get; set; }
        public string Name { get; set; }
    }
}
