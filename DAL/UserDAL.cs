﻿using VcfoApi.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using VcfoApi.Data;
using VcfoApi.DAL.Interface;
using System.Threading.Tasks;

namespace VcfoApi.DAL
{
    public class UserDAL : IUserDAL
    {

        public List<User> ValidLogin(User obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("p_LoginID", DbType.String, ParameterDirection.Input, 0, obj.LoginID);
            objSql.AddParameter("p_Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
            return objSql.ContructList<User>(objSql.ExecuteDataSet("sp_UserLogin"));
        }

        public List<User> SaveUser(User obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("p_name", DbType.String, ParameterDirection.Input, 0, obj.Name);
            objSql.AddParameter("p_EmailId", DbType.String, ParameterDirection.Input, 0, obj.EmailId);
            objSql.AddParameter("p_mobile", DbType.String, ParameterDirection.Input, 0, obj.Mobile);
            int UserId = Convert.ToInt32(objSql.ExecuteScaller("sp_User_ins"));

            var lst = new List<User>() {
                new User(){
                UserId = UserId,
                Name = obj.Name,
                EmailId = obj.EmailId,
                Mobile = obj.Mobile
            }
            };
            return lst;
        }

        public int addOrder(Orders obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("UserId", DbType.Int32, ParameterDirection.Input, 0, obj.UserId);
            objSql.AddParameter("ServiceCode", DbType.String, ParameterDirection.Input, 0, obj.ServiceCode);
            objSql.AddParameter("orderNumber", DbType.String, ParameterDirection.Input, 0, obj.orderNumber);

            objSql.AddParameter("orderDate", DbType.DateTime, ParameterDirection.Input, 0, obj.order_date);
            objSql.AddParameter("GSTNo", DbType.String, ParameterDirection.Input, 0, obj.GSTNo);
            objSql.AddParameter("BusinessName", DbType.String, ParameterDirection.Input, 0, obj.BusinessName);
            objSql.AddParameter("state", DbType.Int32, ParameterDirection.Input, 0, obj.state);

            objSql.AddParameter("country", DbType.String, ParameterDirection.Input, 0, obj.country);
            objSql.AddParameter("pg_resStatus", DbType.String, ParameterDirection.Input, 0, obj.pg_resStatus);
            objSql.AddParameter("pg_resCode", DbType.String, ParameterDirection.Input, 0, obj.pg_resCode);

            objSql.AddParameter("amount", DbType.Decimal, ParameterDirection.Input, 0, obj.amount);
            objSql.AddParameter("net_amount", DbType.Decimal, ParameterDirection.Input, 0, obj.net_amount);
            objSql.AddParameter("tax", DbType.Decimal, ParameterDirection.Input, 0, obj.tax);

            return Convert.ToInt32(objSql.ExecuteScaller("sp_Order_ins"));
        }


        public List<User> GetAllUsers()
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            ds = objSql.ExecuteDataSet("sp_User_sel");
            var lst = (from d in ds.Tables[0].AsEnumerable()
                       select new User
                       {
                           UserId = (int)d["UserId"],
                           Name = !Convert.IsDBNull(d["NAME"]) ? (string?)d["NAME"] : null,
                           EmailId = !Convert.IsDBNull(d["EmailId"]) ? (string?)d["EmailId"] : null,
                           Mobile = !Convert.IsDBNull(d["mobile"]) ? (string?)d["mobile"] : null,
                           CreatedDate = !Convert.IsDBNull(d["created_date"]) ? Convert.ToString(d["created_date"]) : null,
                       }).ToList();
            return lst;
        }

        public List<Orders> GetAllOrders(Orders obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("p_StartDate", DbType.DateTime, ParameterDirection.Input, 0, Convert.ToDateTime(obj.StartDate + " 00:00:00"));
            objSql.AddParameter("p_EndDate", DbType.DateTime, ParameterDirection.Input, 0, Convert.ToDateTime(obj.EndDate + " 23:59:59"));
            return objSql.ContructList<Orders>(objSql.ExecuteDataSet("sp_Order_selByDate"));
            //var lst = (from d in ds.Tables[0].AsEnumerable()
            //           select new Orders
            //           {
            //               id = (int)d["id"],
            //               order_date = Convert.ToString(d["order_date"]),
            //               amount = Convert.ToDecimal(d["amount"]),
            //               tax = Convert.ToDecimal(d["tax"]),
            //               discount = Convert.ToString(d["discount"]),
            //               net_amount = Convert.ToDecimal(d["net_amount"]),
            //               mobile_no = Convert.ToString(d["mobile_no"]),
            //               email = Convert.ToString(d["email"]),
            //               first_name = Convert.ToString(d["first_name"]),
            //               last_name = Convert.ToString(d["last_name"]),
            //               Status = Convert.ToString(d["STATUS"]),
            //               pg_resCode = Convert.ToString(d["pg_resCode"]),
            //               pg_resStatus = Convert.ToString(d["pg_resStatus"]),
            //               pg_resMessage = Convert.ToString(d["pg_resMessage"]),
            //           }).ToList();
            //return lst;
        }

        public List<User> CheckMobileAlreadyRegisteredOrNot(User obj)
        {
            try
            {
                SQL objSql = new SQL();
                DataSet ds = new DataSet();
                objSql.AddParameter("p_MobileNo", DbType.String, ParameterDirection.Input, 0, obj.Mobile);
                ds = objSql.ExecuteDataSet("sp_Users_sel_MobileNo");
                var lst = (from d in ds.Tables[0].AsEnumerable()
                           select new User
                           {
                               Mobile = !Convert.IsDBNull(d["mobile"]) ? (string?)d["mobile"] : null
                           }).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public int InsertOtp(OtpLog obj)
        {
            try
            {
                SQL objSql = new SQL();
                DataSet ds = new DataSet();
                objSql.AddParameter("p_MobileNo", DbType.String, ParameterDirection.Input, 0, obj.Mobile);
                objSql.AddParameter("p_OTP", DbType.String, ParameterDirection.Input, 0, obj.OTP);
                objSql.AddParameter("p_SessionId", DbType.String, ParameterDirection.Input, 0, obj.SessionId);
                return Convert.ToInt32(objSql.ExecuteScaller("sp_OtpLog_ins"));
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<User> Verifymobileotp(OtpLog obj)
        {
            try
            {
                List<User> lst = new List<User>();
                SQL objSql = new SQL();
                DataSet ds = new DataSet();
                objSql.AddParameter("p_MobileNo", DbType.String, ParameterDirection.Input, 0, obj.Mobile);
                objSql.AddParameter("p_OTP", DbType.String, ParameterDirection.Input, 0, obj.OTP);
                int res= Convert.ToInt32(objSql.ExecuteScaller("sp_CheckOtp_mobile"));
                if (res > 0)
                {
                    DataSet ds1 = new DataSet();
                    SQL objSql1 = new SQL();
                    objSql1.AddParameter("p_mobile", DbType.String, ParameterDirection.Input, 0, obj.Mobile);
                    ds1 = objSql1.ExecuteDataSet("sp_User_selByMobile");
                    lst = (from d in ds1.Tables[0].AsEnumerable()
                               select new User
                               {
                                   UserId = (int)d["UserId"],
                                   Name = !Convert.IsDBNull(d["NAME"]) ? (string?)d["NAME"] : null,
                                   EmailId = !Convert.IsDBNull(d["EmailId"]) ? (string?)d["EmailId"] : null,
                                   Mobile = !Convert.IsDBNull(d["mobile"]) ? (string?)d["mobile"] : null,
                                   CreatedDate = !Convert.IsDBNull(d["created_date"]) ? Convert.ToString(d["created_date"]) : null,
                               }).ToList();
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<LookupDoc> GetLookupDoc()
        {
            try
            {
                SQL objSql = new SQL();
                DataSet ds = new DataSet();
                ds = objSql.ExecuteDataSet("sp_LookupDoc_sel");
                var lst = (from d in ds.Tables[0].AsEnumerable()
                           select new LookupDoc
                           {
                               LookupDocId=Convert.ToInt32(d["LookupDocId"]),
                               Name = Convert.ToString(d["Name"])
                           }).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}