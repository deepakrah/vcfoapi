﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.DAL.Interface;
using VcfoApi.Data;
using VcfoApi.Model;

namespace VcfoApi.DAL
{
    public class CategoryDAL: ICategoryDAL
    {
        public int SaveCategory(Category obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("p_code", DbType.String, ParameterDirection.Input, 0, obj.code);
            objSql.AddParameter("p_name", DbType.String, ParameterDirection.Input, 0, obj.name);
            objSql.AddParameter("p_description", DbType.String, ParameterDirection.Input, 0, obj.description);
            if (obj.id > 0)
            {
                objSql.AddParameter("p_id", DbType.Int32, ParameterDirection.Input, 0, obj.id);
                return Convert.ToInt32(objSql.ExecuteNonQuery("sp_Category_upd"));
            }
            else
                return Convert.ToInt32(objSql.ExecuteScaller("sp_Category_ins"));
        }

        public List<Category> GetAllCategories()
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            ds = objSql.ExecuteDataSet("sp_Category_selAll");
            var lst = (from d in ds.Tables[0].AsEnumerable()
                       select new Category
                       {
                           id = (int)d["id"],
                           code = !Convert.IsDBNull(d["code"]) ? (string?)d["code"] : null,
                           name = !Convert.IsDBNull(d["name"]) ? (string?)d["name"] : null,
                           description = !Convert.IsDBNull(d["description"]) ? (string?)d["description"] : null
                       }).ToList();
            return lst;
        }
    }
}
