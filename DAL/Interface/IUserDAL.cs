﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.Model;

namespace VcfoApi.DAL.Interface
{
    public interface IUserDAL
    {
        List<User> ValidLogin(User obj);
        List<User> SaveUser(User obj);
        List<User> GetAllUsers();
        List<Orders> GetAllOrders(Orders obj);
        int addOrder(Orders obj);
        List<User> CheckMobileAlreadyRegisteredOrNot(User obj);
        int InsertOtp(OtpLog obj);
        List<User> Verifymobileotp(OtpLog obj);
        List<LookupDoc> GetLookupDoc();
    }
}
