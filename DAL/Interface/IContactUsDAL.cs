﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.Model;

namespace VcfoApi.DAL.Interface
{
    public interface IContactUsDAL
    {
        int SaveContactUs(ContactUs obj);
        List<ContactUs> GetContactUs();
    }
}
