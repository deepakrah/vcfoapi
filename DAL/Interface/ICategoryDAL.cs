﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.Model;

namespace VcfoApi.DAL.Interface
{
    public interface ICategoryDAL
    {
        int SaveCategory(Category obj);
        List<Category> GetAllCategories();
    }
}
