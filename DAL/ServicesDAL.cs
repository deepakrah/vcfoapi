﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.DAL.Interface;
using VcfoApi.Data;
using VcfoApi.Model;

namespace VcfoApi.DAL
{
    public class ServicesDAL: IServicesDAL
    {
        public int SaveServices(Services obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("p_code", DbType.String, ParameterDirection.Input, 0, obj.code);
            objSql.AddParameter("p_name", DbType.String, ParameterDirection.Input, 0, obj.name);
            objSql.AddParameter("p_price", DbType.String, ParameterDirection.Input, 0, obj.price);
            objSql.AddParameter("p_category_id", DbType.String, ParameterDirection.Input, 0, obj.category_id);
            if (obj.id > 0)
            {
                objSql.AddParameter("p_id", DbType.Int32, ParameterDirection.Input, 0, obj.id);
                return Convert.ToInt32(objSql.ExecuteNonQuery("sp_Service_upd"));
            }
            else
                return Convert.ToInt32(objSql.ExecuteScaller("sp_Service_ins"));
        }

        public List<Services> GetAllServices()
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            ds = objSql.ExecuteDataSet("sp_Service_selAll");
            var lst = (from d in ds.Tables[0].AsEnumerable()
                       select new Services
                       {
                           id = (int)d["id"],
                           code = !Convert.IsDBNull(d["code"]) ? (string?)d["code"] : null,
                           name = !Convert.IsDBNull(d["name"]) ? (string?)d["name"] : null,
                           price = !Convert.IsDBNull(d["price"]) ? Convert.ToString(d["price"]) : null,
                           category_id = !Convert.IsDBNull(d["category_id"]) ? Convert.ToString(d["category_id"]) : null,
                           categoryName = !Convert.IsDBNull(d["categoryName"]) ? (string?)d["categoryName"] : null
                       }).ToList();
            return lst;
        }

        public List<Services> GetServicesById(Services obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("p_id", DbType.Int32, ParameterDirection.Input, 0, obj.id);
            ds = objSql.ExecuteDataSet("sp_Service_selById");
            var lst = (from d in ds.Tables[0].AsEnumerable()
                       select new Services
                       {
                           id = (int)d["id"],
                           code = !Convert.IsDBNull(d["code"]) ? (string?)d["code"] : null,
                           name = !Convert.IsDBNull(d["name"]) ? (string?)d["name"] : null,
                           price = !Convert.IsDBNull(d["price"]) ? Convert.ToString(d["price"]) : null,
                           category_id = !Convert.IsDBNull(d["category_id"]) ? Convert.ToString(d["category_id"]) : null,
                           categoryName = !Convert.IsDBNull(d["categoryName"]) ? (string?)d["categoryName"] : null
                       }).ToList();
            return lst;
        }

        public List<Services> GetServicesByCode(Services obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("p_Code", DbType.String, ParameterDirection.Input, 0, obj.code);
            ds = objSql.ExecuteDataSet("sp_Service_selByCode");
            var lst = (from d in ds.Tables[0].AsEnumerable()
                       select new Services
                       {
                           id = (int)d["id"],
                           code = !Convert.IsDBNull(d["code"]) ? (string?)d["code"] : null,
                           name = !Convert.IsDBNull(d["name"]) ? (string?)d["name"] : null,
                           price = !Convert.IsDBNull(d["price"]) ? Convert.ToString(d["price"]) : null,
                           category_id = !Convert.IsDBNull(d["category_id"]) ? Convert.ToString(d["category_id"]) : null,
                           categoryName = !Convert.IsDBNull(d["categoryName"]) ? (string?)d["categoryName"] : null
                       }).ToList();
            return lst;
        }

        public List<Orders> GetOrderByUserId(Orders obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("p_UserId", DbType.String, ParameterDirection.Input, 0, obj.UserId);
            return  objSql.ContructList<Orders>(objSql.ExecuteDataSet("sp_Order_selByUserId"));
        }
    }
}
