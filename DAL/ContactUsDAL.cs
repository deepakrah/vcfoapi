﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.DAL.Interface;
using VcfoApi.Data;
using VcfoApi.Model;

namespace VcfoApi.DAL
{
    public class ContactUsDAL: IContactUsDAL
    {
        public int SaveContactUs(ContactUs obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("p_Name", DbType.String, ParameterDirection.Input, 0, obj.Name);
            objSql.AddParameter("p_Email", DbType.String, ParameterDirection.Input, 0, obj.Email);
            objSql.AddParameter("p_Subject", DbType.String, ParameterDirection.Input, 0, obj.Subject);
            objSql.AddParameter("p_Message", DbType.String, ParameterDirection.Input, 0, obj.Message);
            return Convert.ToInt32(objSql.ExecuteScaller("sp_Contactus_ins"));
        }
        public List<ContactUs> GetContactUs()
        {
            SQL objSql = new SQL();
            return objSql.ContructList<ContactUs>(objSql.ExecuteDataSet("sp_ContactUs_sel"));
        }
    }
}
