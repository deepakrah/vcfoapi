﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using Microsoft.Extensions.Configuration;
using MySqlConnector;
namespace VcfoApi.Data
{

    public class SQL
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlTransaction transaction;
        string connectionString;
        private readonly IConfiguration Configuration;
        public SQL()
        {
            
            if (String.IsNullOrEmpty(connectionString))
            {
                var configBuilder = new ConfigurationBuilder();
                var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
                configBuilder.AddJsonFile(path);
                var root = configBuilder.Build();
                connectionString = root.GetConnectionString("DataConnection");
            }
            this.Init();
        }
        public void Init()
        {
            if (con == null)
            {
                con = new MySqlConnection(connectionString);
            }
            if (con.State != System.Data.ConnectionState.Closed)
            {
                con.Close();
            }
            try
            {
                con.Open();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if(cmd == null)
            {
                NewCommand();
            }
        }

        public void NewCommand()
        {
            cmd = null;
            var newCmd = new MySqlCommand();
            newCmd.CommandTimeout = 120;
            if (transaction != null)
            {
                newCmd.Transaction = transaction;
            }
            newCmd.Connection = con;
            cmd = newCmd;
        }
        public void Dispose()
        {
            this.con.Dispose();
        }

        public void AddParameter(string name, DbType type, ParameterDirection dir, int size, object value)
        {
            MySqlParameter p = new MySqlParameter();
            p.ParameterName = name;
            p.DbType = type;
            p.Direction = dir;
            p.Direction = dir;
            p.Size = size;
            p.Value = value;
            cmd.Parameters.Add(p);
        }
        public void BeginTransaction()
        {
            transaction = con.BeginTransaction();
        }
        public void Commit()
        {
            transaction.Commit();
        }
        public void Rollback()
        {
            transaction.Rollback();
        }
        public DataSet ExecuteDataSet(string procName)
        {
            SetCMDName(procName);
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;

        }
        void SetCMDName(string procName)
        {
            cmd.CommandText = procName;
            cmd.CommandType = CommandType.StoredProcedure;
        }
        public IDataReader ExecuteReader(string procName)
        {
            SetCMDName(procName);
            IDataReader dr = cmd.ExecuteReader();
            return dr;
        }
        public int ExecuteNonQuery(string procName)
        {
            SetCMDName(procName);
            return cmd.ExecuteNonQuery();
        }
        public Object ExecuteScaller(string procName)
        {
            SetCMDName(procName);
            var obj = cmd.ExecuteScalar();
            return obj;
        }

        public List<T> ContructList<T>(DataSet dr)
        {
            List<T> objList = new List<T>();
            foreach (DataTable dt in dr.Tables)
            {
                foreach (DataRow row in dt.Rows)
                {
                    T objT;
                    Type t = typeof(T);
                    BindingFlags bflags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
                    ConstructorInfo cInfo = typeof(T).GetConstructor(bflags, null, new Type[0] { }, null);
                    if (cInfo != null)
                    {
                        objT = (T)cInfo.Invoke(null);
                    }
                    else
                        objT = Activator.CreateInstance<T>();



                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    for (int col = 0; col <= dt.Columns.Count - 1; col++)
                    {
                        foreach (PropertyDescriptor prop in properties)
                        {
                            if (prop.Name.ToUpper() == dt.Columns[col].ColumnName.ToUpper() && !string.IsNullOrEmpty(Convert.ToString(row[col])))
                            {
                                var underlyingType = Nullable.GetUnderlyingType(prop.PropertyType);
                                if (underlyingType == null)
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], prop.PropertyType));

                                }
                                else
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], underlyingType));


                                }
                            }
                        }
                    }

                    objList.Add(objT);
                }
            }
            return objList;


        }
    }
}
