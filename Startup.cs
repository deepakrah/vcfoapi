using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VcfoApi.BAL;
using VcfoApi.BAL.Interface;
using VcfoApi.DAL;
using VcfoApi.DAL.Interface;
using VcfoApi.JWT;

namespace VcfoApi
{
    public class Startup
    {
        public static string ConnectionString
        {
            get;
            private set;
        }
        public static string ServerPath
        {
            get;
            private set;
        }
        public static IWebHostEnvironment _currentEnvironment;
        public Startup(IWebHostEnvironment env)
        {
            Configuration = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appSettings.json").Build();

            _currentEnvironment = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

            //services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            //{
            //    builder
            //    .AllowAnyMethod()
            //    .AllowAnyHeader()
            //    .AllowCredentials()
            //    .AllowAnyOrigin();
            //    //.WithOrigins("http://localhost:4500", "http://localhost:4800", "http://localhost:5300");
            //}));

            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin",
                    options => options
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod());
            });

            //services.AddControllers();
            services.AddSingleton<IFileProvider>(
                new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));

            services.AddMvc();
            services.AddTransient<IUserBAL, UserBAL>();
            services.AddScoped<IUserDAL, UserDAL>();

            services.AddTransient<ICategoryBAL, CategoryBAL>();
            services.AddScoped<ICategoryDAL, CategoryDAL>();

            services.AddTransient<IServicesBAL, ServicesBAL>();
            services.AddScoped<IServicesDAL, ServicesDAL>();

            services.AddTransient<IContactUsBAL, ContactUsBAL>();
            services.AddScoped<IContactUsDAL, ContactUsDAL>();

            services.AddHttpContextAccessor();

            //var appSettingsSection = Configuration.GetSection("AppSettings");
            //services.Configure<ApplicationSettings>(appSettingsSection);
            //var appSettings = appSettingsSection.Get<ApplicationSettings>();

            //*************************JWT Authentication Start here****************************

            var key = Encoding.UTF8.GetBytes(Configuration["ApplicationSettings:Jwt_Secret"].ToString());

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(x =>
           {
               x.RequireHttpsMetadata = false;
               x.SaveToken = true;
               x.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuerSigningKey = true,
                   IssuerSigningKey = new SymmetricSecurityKey(key),
                   ValidateIssuer = false,
                   ValidateAudience = false
               };
           });

            services.AddScoped<IAuthorizeService, AuthorizeService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //*************************JWT Authentication End here****************************
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(options => options
              .AllowAnyOrigin()
              .AllowAnyHeader()
              .AllowAnyMethod()
             );
            app.UseRouting();

            app.UseAuthentication();//JWT Authentication
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseStaticFiles(); // For the wwwroot folder
            ConnectionString = Configuration["ConnectionStrings:DefaultConnection"];
            ServerPath = Configuration["EmailSetting:ServerPath"];
        }
    }
}
