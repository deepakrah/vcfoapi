﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.BAL.Interface;
using VcfoApi.DAL.Interface;
using VcfoApi.Model;

namespace VcfoApi.BAL
{
    public class CategoryBAL: ICategoryBAL
    {
        ICategoryDAL _categoryDAL;
        public CategoryBAL(ICategoryDAL categoryDAL)
        {
            _categoryDAL = categoryDAL;
        }
        public int SaveCategory(Category obj)
        {
            return _categoryDAL.SaveCategory(obj);
        }
        public List<Category> GetAllCategories()
        {
            return _categoryDAL.GetAllCategories();
        }
    }
}
