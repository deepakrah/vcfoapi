﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.BAL.Interface;
using VcfoApi.DAL.Interface;
using VcfoApi.Model;

namespace VcfoApi.BAL
{
    public class ServicesBAL : IServicesBAL
    {
        IServicesDAL _ServicesDAL;
        public ServicesBAL(IServicesDAL ServicesDAL)
        {
            _ServicesDAL = ServicesDAL;
        }
        public int SaveServices(Services obj)
        {
            return _ServicesDAL.SaveServices(obj);
        }
        public List<Services> GetAllServices()
        {
            return _ServicesDAL.GetAllServices();
        }
        public List<Services> GetServicesById(Services obj)
        {
            return _ServicesDAL.GetServicesById(obj);
        }
        public List<Services> GetServicesByCode(Services obj)
        {
            return _ServicesDAL.GetServicesByCode(obj);
        }
        public List<Orders> GetOrderByUserId(Orders obj)
        {
            return _ServicesDAL.GetOrderByUserId(obj);
        }
    }
}