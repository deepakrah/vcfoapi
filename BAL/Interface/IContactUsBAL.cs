﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.Model;

namespace VcfoApi.BAL.Interface
{
    public interface IContactUsBAL
    {
        int SaveContactUs(ContactUs obj);
        List<ContactUs> GetContactUs();
    }
}
