﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.Model;

namespace VcfoApi.BAL.Interface
{
    public interface IServicesBAL
    {
        int SaveServices(Services obj);
        List<Services> GetAllServices();
        List<Services> GetServicesById(Services obj);
        List<Services> GetServicesByCode(Services obj);
        List<Orders> GetOrderByUserId(Orders obj);
    }
}
