﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.Model;

namespace VcfoApi.BAL.Interface
{
    public interface ICategoryBAL
    {
        int SaveCategory(Category obj);
        List<Category> GetAllCategories();
    }
}
