﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.BAL.Interface;
using VcfoApi.DAL.Interface;
using VcfoApi.Model;

namespace VcfoApi.BAL
{
    public class ContactUsBAL: IContactUsBAL
    {
        IContactUsDAL _contactUsDAL;
        public ContactUsBAL(IContactUsDAL contactUsDAL)
        {
            _contactUsDAL = contactUsDAL;
        }
        public int SaveContactUs(ContactUs obj)
        {
            return _contactUsDAL.SaveContactUs(obj);
        }
        public List<ContactUs> GetContactUs()
        {
            return _contactUsDAL.GetContactUs();
        }
    }
}
