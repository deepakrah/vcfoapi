﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using VcfoApi.DAL;
using VcfoApi;
using VcfoApi.BAL;
using VcfoApi.BAL.Interface;
using VcfoApi.Model;
using VcfoApi.DAL.Interface;

namespace VcfoApi.BAL
{
    public class UserBAL : IUserBAL
    {
        IUserDAL _users;

        public UserBAL(IUserDAL users)
        {
            _users = users;
        }

        public List<User> ValidLogin(User obj)
        {

            return _users.ValidLogin(obj);
        }

        public List<User> SaveUser(User obj)
        {
            return _users.SaveUser(obj);
        }

        public int addOrder(Orders obj)
        {
            return _users.addOrder(obj);
        }


        public List<User> GetAllUsers()
        {
            return _users.GetAllUsers();
        }
        public List<Orders> GetAllOrders(Orders obj)
        {
            return _users.GetAllOrders(obj);
        }

        public List<User> CheckMobileAlreadyRegisteredOrNot(User obj)
        {
            return _users.CheckMobileAlreadyRegisteredOrNot(obj);
        }
        public int InsertOtp(OtpLog obj)
        {
            return _users.InsertOtp(obj);
        }
        public List<User> Verifymobileotp(OtpLog obj)
        {
            return _users.Verifymobileotp(obj);
        }
        public List<LookupDoc> GetLookupDoc()
        {
            return _users.GetLookupDoc();
        }
    }

}