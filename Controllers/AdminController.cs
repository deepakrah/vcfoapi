﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using VcfoApi.BAL.Interface;
using VcfoApi.Controllers.Common;
using VcfoApi.JWT;
using VcfoApi.Model;

namespace VcfoApi.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/Admin")]
    public class AdminController : Controller
    {
        IUserBAL _userBAL;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly ApplicationSettings _appSettings;
        Utilities _utilities = new Utilities();
        public static string webRootPath;
        public IActionResult Index()
        {
            return View();
        }

        public AdminController(IUserBAL userBAL, IWebHostEnvironment hostingEnvironment, IOptions<ApplicationSettings> appSettings)
        {
            _hostingEnvironment = hostingEnvironment;
            _userBAL = userBAL;
            _appSettings = appSettings.Value;
            webRootPath = hostingEnvironment.WebRootPath;
        }


        [HttpPost]
        [Route("ValidateUser")]
        [AllowAnonymous]
        public List<User> ValidateUser([FromBody] User objUser)
        {

            try
            {
                List<User> lstLogin = new List<User>();
                lstLogin = this._userBAL.ValidLogin(objUser);

                if (lstLogin.Count > 0)
                {
                    AuthorizeService auth = new AuthorizeService();
                    string _token = auth.Authenticate(Convert.ToString(lstLogin[0].UserId), _appSettings);
                    lstLogin[0].Token = _token;
                }

                return lstLogin;

            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }

        }

        [HttpPost]
        [Route("SaveUser")]
        [AllowAnonymous]
        public List<User> SaveUser([FromBody] User objUser)
        {

            try
            {
                //return this._userBAL.SaveUser(objUser);

                List<User> lstLogin = new List<User>();
                lstLogin = this._userBAL.SaveUser(objUser);

                if (lstLogin.Count > 0)
                {
                    AuthorizeService auth = new AuthorizeService();
                    string _token = auth.Authenticate(Convert.ToString(lstLogin[0].UserId), _appSettings);
                    lstLogin[0].Token = _token;
                }

                return lstLogin;
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }

        }

        [HttpPost]
        [Route("addOrder")]
        [AllowAnonymous]
        public int addOrder([FromBody] Orders objorder)
        {

            try
            {
                return this._userBAL.addOrder(objorder);
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return -1;
            }

        }

        [HttpPost]
        [Route("GetAllUsers")]
        //[AllowAnonymous]
        public List<User> GetAllUsers()
        {
            try
            {
                return this._userBAL.GetAllUsers();
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpPost]
        [Route("GetAllOrders")]
        //[AllowAnonymous]
        public List<Orders> GetAllOrders([FromBody] Orders obj)
        {
            try
            {
                return this._userBAL.GetAllOrders(obj);
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpPost]
        [Route("CheckMobileAllReadyRegisteredOrNot")]
        [AllowAnonymous]
        public int CheckMobileAllReadyRegisteredOrNot([FromBody] User obj)
        {
            try
            {
                List<User> lstuser = new List<User>();
                lstuser = this._userBAL.CheckMobileAlreadyRegisteredOrNot(obj);
                string urlParameters = "";

                if (lstuser.Count > 0)
                {
                    Random _random = new Random();
                    int otp = _random.Next(100000, 999999);
                    //string api_key = "c47c40de-e3cf-11ea-9fa5-0200cd936042";

                    //string URL = "https://2factor.in/API/V1/" + api_key + "/SMS/+91" + obj.Mobile.ToString() + "/" + otp.ToString();

                    //HttpClient client = new HttpClient();
                    //client.BaseAddress = new Uri(URL);

                    //// Add an Accept header for JSON format.
                    //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //// List data response.
                    //HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                    //if (response.IsSuccessStatusCode)
                    //{
                    //    var jsonData = response.Content.ReadAsStringAsync();
                    //    dynamic data = JObject.Parse(jsonData.Result);
                    //    string OTPsessionid = ((Newtonsoft.Json.Linq.JValue)((Newtonsoft.Json.Linq.JProperty)((Newtonsoft.Json.Linq.JContainer)data).Last).Value).Value.ToString();
                    string OTPsessionid = "";
                    OtpLog _objOtpLog = new OtpLog();
                    _objOtpLog.Mobile = obj.Mobile.ToString();
                    _objOtpLog.OTP = otp.ToString();
                    _objOtpLog.SessionId = OTPsessionid;

                    int res = this._userBAL.InsertOtp(_objOtpLog);

                    return res;
                    //}
                    //else
                    //{
                    //    return -1;
                    //    //return 0;
                    //}
                }
                return lstuser.Count;

            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        [HttpPost]
        [Route("VerifyMobileOtp")]
        [AllowAnonymous]
        public List<User> VerifyMobileOtp([FromBody] OtpLog obj)
        {
            try
            {
                //int res = this._userBAL.Verifymobileotp(obj);
                //if (res > 0)
                //{
                //    return 1;
                //}
                //else
                //{
                //    return 0;
                //}

                List<User> lstLogin = new List<User>();
                lstLogin = this._userBAL.Verifymobileotp(obj);

                if (lstLogin.Count > 0)
                {
                    AuthorizeService auth = new AuthorizeService();
                    string _token = auth.Authenticate(Convert.ToString(lstLogin[0].UserId), _appSettings);
                    lstLogin[0].Token = _token;
                }
                return lstLogin;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("GetLookupDoc")]
        [AllowAnonymous]
        public List<LookupDoc> GetLookupDoc()
        {
            try
            {
                return this._userBAL.GetLookupDoc();
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpPost]
        [Route("UploadDoc")]
        public List<string> UploadDoc([FromBody] User obj)
        {
            try
            {
                if (obj.FileSource != null)
                    _utilities.SaveDocument(obj.UserId, obj.FileSource, webRootPath, "\\Documents\\", obj.Name, obj.OrderId);
                List<string> lst = new List<string>();
                string[] str = this._utilities.DocPath(obj.UserId, webRootPath, "\\Documents\\", obj.OrderId);
                lst.AddRange(str);
                return lst;
            }
            catch (Exception ex)
            {
                //Logger.LogError($"Something went wrong inside EmployeeController UploadExtension action: {ex.Message}");
                return null;
            }
        }

        [HttpPost]
        [Route("DocPath")]
        //[AllowAnonymous]
        public List<string> DocPath([FromBody] User obj)
        {
            try
            {
                List<string> lst = new List<string>();
                string[] str = this._utilities.DocPath(obj.UserId, webRootPath, "\\Documents\\", obj.OrderId);
                lst.AddRange(str);
                return lst;
            }
            catch (Exception ex)
            {
                //Logger.LogError($"Something went wrong inside EmployeeController GetExtensionPath action: {ex.Message}");
                return null;
            }
        }

    }
}
