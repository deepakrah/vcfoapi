﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.BAL.Interface;
using VcfoApi.JWT;
using VcfoApi.Model;

namespace VcfoApi.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("api/ContactUs")]
    public class ContactUsController : Controller
    {
        IContactUsBAL _contactUsBAL;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly ApplicationSettings _appSettings;
        public IActionResult Index()
        {
            return View();
        }

        public ContactUsController(IContactUsBAL contactUsBAL, IWebHostEnvironment hostingEnvironment, IOptions<ApplicationSettings> appSettings)
        {
            _hostingEnvironment = hostingEnvironment;
            _contactUsBAL = contactUsBAL;
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        [Route("SaveContactUs")]
        [AllowAnonymous]
        public int SaveContactUs([FromBody] ContactUs objContactUs)
        {
            try
            {
                return _contactUsBAL.SaveContactUs(objContactUs);
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return -1;
            }
        }

        [HttpPost]
        [Route("GetContactUs")]
        [AllowAnonymous]
        public List<ContactUs> GetContactUs()
        {
            try
            {
                return this._contactUsBAL.GetContactUs();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
