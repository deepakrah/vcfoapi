﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.BAL.Interface;
using VcfoApi.JWT;
using VcfoApi.Model;

namespace VcfoApi.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/Services")]
    public class ServicesController : Controller
    {
        IServicesBAL _ServicesBAL;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly ApplicationSettings _appSettings;
        public IActionResult Index()
        {
            return View();
        }

        public ServicesController(IServicesBAL ServicesBAL, IWebHostEnvironment hostingEnvironment, IOptions<ApplicationSettings> appSettings)
        {
            _hostingEnvironment = hostingEnvironment;
            _ServicesBAL = ServicesBAL;
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        [Route("SaveServices")]
        //[AllowAnonymous]
        public int SaveServices([FromBody] Services objServices)
        {
            try
            {
                return _ServicesBAL.SaveServices(objServices);
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return -1;
            }
        }

        [HttpPost]
        [Route("GetAllServices")]
        [AllowAnonymous]
        public List<Services> GetAllServices()
        {
            try
            {
                List<Services> lstLogin = new List<Services>();
                lstLogin = this._ServicesBAL.GetAllServices();
                return lstLogin;
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpPost]
        [Route("GetServicesById")]
        [AllowAnonymous]
        public List<Services> GetServicesById([FromBody] Services objServices)
        {
            try
            {
                List<Services> lstLogin = new List<Services>();
                lstLogin = this._ServicesBAL.GetServicesById(objServices);
                return lstLogin;
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpPost]
        [Route("GetServicesByCode")]
        [AllowAnonymous]
        public List<Services> GetServicesByCode([FromBody] Services objServices)
        {
            try
            {
                List<Services> lstLogin = new List<Services>();
                lstLogin = this._ServicesBAL.GetServicesByCode(objServices);
                return lstLogin;
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpPost]
        [Route("GetOrderByUserId")]
        //[AllowAnonymous]
        public List<Orders> GetOrderByUserId([FromBody] Orders objServices)
        {
            try
            {
                return this._ServicesBAL.GetOrderByUserId(objServices);
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }
    }
}