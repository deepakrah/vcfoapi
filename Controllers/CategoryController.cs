﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VcfoApi.BAL.Interface;
using VcfoApi.JWT;
using VcfoApi.Model;

namespace VcfoApi.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/Category")]
    public class CategoryController : Controller
    {
        ICategoryBAL _categoryBAL;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly ApplicationSettings _appSettings;
        public IActionResult Index()
        {
            return View();
        }

        public CategoryController(ICategoryBAL categoryBAL, IWebHostEnvironment hostingEnvironment, IOptions<ApplicationSettings> appSettings)
        {
            _hostingEnvironment = hostingEnvironment;
            _categoryBAL = categoryBAL;
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        [Route("SaveCategory")]
        //[AllowAnonymous]
        public int SaveCategory([FromBody] Category objCategory)
        {
            try
            {
                return _categoryBAL.SaveCategory(objCategory);
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return -1;
            }
        }

        [HttpPost]
        [Route("GetAllCategories")]
        [AllowAnonymous]
        public List<Category> GetAllCategories()
        {
            try
            {
                List<Category> lstLogin = new List<Category>();
                lstLogin = this._categoryBAL.GetAllCategories();
                return lstLogin;
            }
            catch (Exception ex)
            {
                //ErrorLogger.Log($"Something went wrong inside UsersController ValidLogin action: {ex.Message}");
                //ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }
    }
}
