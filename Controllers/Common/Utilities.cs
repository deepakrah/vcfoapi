﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace VcfoApi.Controllers.Common
{
    public class Utilities
    {
        public void SaveDocument(int Id, string[] FileSource, string WebRootPath, string Path, string FileName, int OrderId)
        {
            if (Id > 0)
            {
                if (FileSource.Length > 0)
                {
                    string FolderPath = WebRootPath + Path + Id + "\\" + OrderId + "\\";
                    bool folderExists = System.IO.Directory.Exists(FolderPath);
                    if (!folderExists)
                        Directory.CreateDirectory(FolderPath);
                    DirectoryInfo directory = new DirectoryInfo(FolderPath);
                    //foreach (FileInfo file in directory.GetFiles())
                    //{
                    //    file.Delete();
                    //}
                    for (int i = 0; i < FileSource.Length; i++)
                    {
                        string filename = "";
                        if (FileSource[i].Contains("data:application/pdf;base64,"))
                        {
                            filename = FileName + "_" + DateTime.Now.ToString("dd-MM-yyyy HHmmss") + ".pdf";
                        }
                        else if (FileSource[i].Contains("data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,"))
                        {
                            filename = FileName + "_" + DateTime.Now.ToString("dd-MM-yyyy HHmmss") + ".docx";
                        }
                        else
                        {
                            filename = FileName + "_" + DateTime.Now.ToString("dd-MM-yyyy HHmmss") + ".jpg";
                        }
                        string fileNameWitPath = FolderPath + filename;
                        using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs))
                            {
                                if (FileSource[i].Contains("data:image/jpeg;base64,"))
                                {
                                    byte[] data = Convert.FromBase64String(FileSource[i].Replace("data:image/jpeg;base64,", ""));
                                    bw.Write(data);
                                    bw.Close();
                                }
                                if (FileSource[i].Contains("data:image/jpg;base64,"))
                                {
                                    byte[] data = Convert.FromBase64String(FileSource[i].Replace("data:image/jpg;base64,", ""));
                                    bw.Write(data);
                                    bw.Close();
                                }
                                if (FileSource[i].Contains("data:image/png;base64,"))
                                {
                                    byte[] data = Convert.FromBase64String(FileSource[i].Replace("data:image/png;base64,", ""));
                                    bw.Write(data);
                                    bw.Close();
                                }
                                if (FileSource[i].Contains("data:application/pdf;base64,"))
                                {
                                    byte[] data = Convert.FromBase64String(FileSource[i].Replace("data:application/pdf;base64,", ""));
                                    bw.Write(data);
                                    bw.Close();
                                }
                                if (FileSource[i].Contains("data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,"))
                                {
                                    byte[] data = Convert.FromBase64String(FileSource[i].Replace("data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,", ""));
                                    bw.Write(data);
                                    bw.Close();
                                }
                            }
                        }
                    }

                }
            }
        }

        public string[] DocPath(int Id, string WebRootPath, string Path, int OrderId)
        {
            string[] base64ImageRepresentation = new string[0];
            string folderPath;

            folderPath = WebRootPath + Path + Id + "\\" + OrderId + "\\";
            //string folderPath = _hostingEnvironment.WebRootPath + "\\uccImages\\User\\" + UserId + "\\";
            if (Directory.Exists(folderPath))
            {
                string[] AllFiles = Directory.GetFiles(folderPath, "*", SearchOption.AllDirectories);
                //int fCount = Directory.GetFiles(folderPath, "*", SearchOption.AllDirectories).Length;
                base64ImageRepresentation = new string[AllFiles.Length];
                for (int i = 0; i < AllFiles.Length; i++)
                {
                    //byte[] imageArray = System.IO.File.ReadAllBytes(AllFiles[i]);
                    //base64ImageRepresentation[i] = "data:image/jpeg;base64," + Convert.ToBase64String(imageArray);
                    base64ImageRepresentation[i] = AllFiles[i].Split('\\').LastOrDefault();
                }
            }
            return base64ImageRepresentation;
        }
    }
}
